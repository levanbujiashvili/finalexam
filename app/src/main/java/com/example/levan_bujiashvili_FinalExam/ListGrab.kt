package com.example.levan_bujiashvili_FinalExam

import com.google.gson.annotations.SerializedName

class ListGrab {
    @SerializedName("name")
    var name: String? = null
    @SerializedName("realname")
    var realName: String? = null
    @SerializedName("imageurl")
    var imageUrl: String? = null
    @SerializedName("bio")
    var bio: String? = null
}